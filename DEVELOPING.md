# Creating a new version of atlas-dnf5

You can just build from the `Dockerfile` to create the image yourself.

You can pass the `--build-arg DFN5_BRANCH=...` option to docker build
to change the dnf5 branch you want to use.

```shell
docker build --build-arg DFN5_BRANCH=atlas-branch-5.1.3 -t 5.1.3 .
```

These instructions are for creating a new version
and potentially debugging it.

## Environment

Create an environment based on Fedora 39, e.g. via
docker/podman, LXD, or any other virtualization tool.
You should have root access inside the environment.

```shell
podman run -it fedora:39
```

or

```shell
lxc launch images:fedora/39 f39
lxc shell f38
```

Install the basic development tools.

```shell
dnf -y install git 'dnf-command(builddep)'
```

## Rebase to latest upstream tag

Create a *new* branch of the form `atlas-branch-5.x.y`
based on the latest existing atlas branch, e.g.

```shell
git clone https://github.com/atlas-tdaq-software/dnf5.git /var/tmp/dnf5
cd /var/tmp/dnf5
git branch atlas-branch-5.x.y atlas-branch-5.1.13
```

Now rebase on top of the upstream tag:

```shell
git checkout atlas-branch-5.x.y
git rebase 5.x.y
```

Fix all merge conflicts.

## Build it

```shell
dnf -y builddep /var/tmp/dnf5/dnf5.spec
mkdir /var/tmp/build && cd /var/tmp/build
cmake \
 -D WITH_DNF5DAEMON_CLIENT=OFF \
 -D WITH_DNF5DAEMON_SERVER=OFF \
 -D WITH_DNF5DAEMON_TESTS=OFF  \
 -D WITH_DNF5_PLUGINS=OFF      \
 -D WITH_MAN=OFF               \
 -D WITH_PERFORMANCE_TESTS=OFF \
 -D WITH_PERL5=OFF             \
 -D WITH_PLUGIN_ACTIONS=OFF    \
 -D WITH_PYTHON3=OFF           \
 -D WITH_PYTHON_PLUGINS_LOADER=OFF \
 -D WITH_RUBY=OFF              \
 -D WITH_SANITIZERS=OFF        \
 -D WITH_TESTS=OFF             \
 /var/tmp/dnf5
make -j $(nproc)
make install
```

Fix all build errors.

## Adjust atlas-dnf5

In the `Dockerfile` and `build.sh` adjust the branch to the new version.

## Release

If all looks fine, tag `atlas-dnf5` with a new tag of
the form `v5.x.y`. A new release will be automatically created.

## RHEL 9 native version

```shell
git clone https://gitlab.cern.ch/atlas-tdaq-software/atlas-dnf5.git
podman run -it  --rm -v $(pwd):$(pwd) -w $(pwd) gitlab-registry.cern.ch/linuxsupport/alma9-base:latest
./build.sh
```

The `dist` directory will contain the build results. It can be tar'ed up and distributed.
