#!/bin/bash

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# Run this script only in an environment you can throw away
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

[ $(id -u) -ne 0 ] && SUDO=sudo

export DNF5_BRANCH=${DNF5_BRANCH:-atlas-branch-5.2.10.0}
export PREFIX=${PREFIX:-/usr/local}
mkdir -p $PREFIX
export PKG_CONFIG_PATH=${PREFIX}/lib64/pkgconfig

set -e

# update prepare system
$SUDO dnf -y install epel-release
$SUDO dnf -y --enablerepo="epel,crb" install --skip-broken tcl libcurl-devel lua-devel libarchive-devel file-devel libgcrypt-devel fmt-devel gpgme-devel cmake gcc-c++ swig  'dnf-command(builddep)' git libsmartcols-devel bzip2-devel bzip2 openssl-devel popt-devel doxygen libxml2-devel libzstd-devel libmodulemd-devel bash-completion diffutils gettext toml11-devel
[ ! -d dnf5 ] && git clone -b ${DNF5_BRANCH} https://github.com/rhauser/dnf5.git || (cd dnf5; git checkout ${DNF5_BRANCH})
$SUDO dnf -y --enablerepo="crb" builddep --skip-unavailable dnf5/dnf5.spec

export LD_LIBRARY_PATH=${PREFIX}/lib:${PREFIX}/lib64

### libsolv 0.7.30, needed for dnf5 >= 5.2.10.0
curl -L https://github.com/openSUSE/libsolv/archive/refs/tags/0.7.30.tar.gz | tar zxf -
pushd libsolv-0.7.30
mkdir -p build && cd build
cmake \
  -DFEDORA=1                                              \
  -DENABLE_RPMDB=ON                                       \
  -DENABLE_RPMDB_BYRPMHEADER=ON                           \
  -DENABLE_RPMDB_LIBRPM=ON                                \
  -DENABLE_RPMPKG_LIBRPM=ON                               \
  -DENABLE_RPMMD=ON                                       \
  -DUSE_VENDORDIRS=ON                                     \
  -DWITH_LIBXML2=ON                                       \
  -DWITH_OPENSSL=ON                                       \
  -DENABLE_LZMA_COMPRESSION=ON                            \
  -DENABLE_BZIP2_COMPRESSION=ON                           \
  -DENABLE_ZSTD_COMPRESSION=ON                            \
  -DENABLE_COMPS=ON                                       \
  -DENABLE_COMPLEX_DEPS=ON                                \
  -DCMAKE_INSTALL_PREFIX=${PREFIX}                        \
  ..
make -j $(nproc) && $SUDO make -j $(nproc) install
popd

### dnf5

mkdir -p dnf5/build
pushd dnf5/build
export PATH=/usr/bin:/usr/sbin
export CXXFLAGS="-isystem ${PREFIX}/include -L${PREFIX}/lib64"
cmake  -D WITH_DNF5DAEMON_CLIENT=OFF \
       -D WITH_DNF5DAEMON_SERVER=OFF \
       -D WITH_DNF5DAEMON_TESTS=OFF \
       -D WITH_DNF5_PLUGINS=OFF \
       -D WITH_MAN=OFF \
       -D WITH_PERFORMANCE_TESTS=OFF \
       -D WITH_PERL5=OFF \
       -D WITH_PYTHON3=OFF \
       -D WITH_PYTHON_PLUGINS_LOADER=OFF \
       -D WITH_PLUGIN_APPSTREAM=OFF \
       -D WITH_RUBY=OFF \
       -D WITH_SANITIZERS=OFF \
       -D WITH_TESTS=OFF \
       -D WITH_ZCHUNK=OFF \
       -D WITH_MODULEMD=OFF \
       -D WITH_SYSTEMD=OFF \
       -D CMAKE_INSTALL_PREFIX=${PREFIX} \
       ..
# note: needed to find correct librepo to link dnf5 executable
make -j $(nproc)
$SUDO make install
popd

## Usage

export PATH=${PREFIX}/bin:$PATH
dnf5 --help

## build dist dir

# not every target has this installed, so make it a bit easier
$SUDO cp /usr/lib64/libfmt* ${PREFIX}/lib64
$SUDO cp vars/* ${PREFIX}/etc/dnf/vars/

test -f dnf.conf && $SUDO cp dnf.conf ${PREFIX}/etc/dnf
test -f dbpath.conf && $SUDO cp dbpath.conf ${PREFIX}/etc/dnf/libdnf5-plugins
test -d yum.repos.d && $SUDO mkdir -p ${PREFIX}/etc/yum.repos.d && $SUDO cp yum.repos.d/* ${PREFIX}/etc/yum.repos.d
mkdir -p dist/usr
cp -r ${PREFIX} dist/usr
test -f atlas-dnf5.native && cp atlas-dnf5.native dist/atlas-dnf5
cp scripts/* dist/

# test again
export PATH=${DIST}/bin:/usr/bin
export ATLAS_DNF5_INSTALL_DIR=$(pwd)/install
export ATLAS_DNF5_STATE_DIR=$(pwd)/dnf-state
dist/atlas-dnf5 --help
