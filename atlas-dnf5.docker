#!/bin/bash
ATLAS_DNF5_VERSION=${ATLAS_DNF5_VERSION:-latest}
[ -z "$ATLAS_DNF5_INSTALL_DIR" ] && { echo "Set the ATLAS_DNF5_INSTALL_DIR variable to your install area" >&2 ; exit 1; }

entrypoint=
args="${ATLAS_DNF5_PROXY:+--setopt=proxy=${ATLAS_DNF5_PROXY}} $@"
[ "$1" = "--shell" ] && entrypoint="--entrypoint /bin/bash" && args=""

for _arg in $@
do
    case ${_arg} in
        --setvar=OS=*)
            export DNF_VAR_OS_NUMBER=$(echo -n ${_arg#--setvar=OS=} | tail -c 1)
            ;;
        OS=*)
	        export DNF_VAR_OS_NUMBER=$(echo -n ${_arg#OS=} | tail -c 1)
	        ;;
    esac
done

for repo in $ATLAS_DNF5_REPOS
do
    REPOS_MOUNTS="${REPOS_MOUNTS} -v $(readlink -f ${repo}):/etc/yum.repos.d/$(basename ${repo})"
done

docker run $entrypoint -it --rm \
   --user $(id -u):$(id -g) \
   -v ${ATLAS_DNF5_HOST_INSTALL_DIR:-$ATLAS_DNF5_INSTALL_DIR}:$ATLAS_DNF5_INSTALL_DIR \
   -v ${ATLAS_DNF5_STATE_DIR:-/var/tmp}:/var/tmp \
   ${ATLAS_DNF5_REPOS_DIR:+-v ${ATLAS_DNF5_REPOS_DIR}:/etc/yum.repos.d} \
   ${REPOS_MOUNTS} \
   --env DNF_VAR_PREFIX=$ATLAS_DNF5_INSTALL_DIR \
   ${DNF_VAR_BRANCH:+--env DNF_VAR_BRANCH=${DNF_VAR_BRANCH}} \
   ${DNF_VAR_ARCH:+--env DNF_VAR_ARCH=${DNF_VAR_ARCH}} \
   ${DNF_VAR_OS:+--env DNF_VAR_OS=${DNF_VAR_OS}} \
   ${DNF_VAR_OS:+--env DNF_VAR_OS_NUMBER=$(echo -n ${DNF_VAR_OS} | tail -c 1)} \
   ${DNF_VAR_OS_NUMBER:+--env DNF_VAR_OS_NUMBER=${DNF_VAR_OS_NUMBER}} \
   ${DNF_VAR_LCG:+--env DNF_VAR_LCG=${DNF_VAR_LCG}} \
   ${DNF_VAR_HTTP_PROXY:+env DNF_VAR_HTTP_PROXY=${DNF_VAR_HTTP_PROXY}} \
   ${ATLAS_DNF5_IMAGE:-gitlab-registry.cern.ch/atlas-tdaq-software/atlas-dnf5}:${ATLAS_DNF5_VERSION} \
   $args
