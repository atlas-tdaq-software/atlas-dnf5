# DNF5 for ATLAS

This project contains a patched version of [DNF5](https://github.com/rpm-software-management/dnf5) for
the use in [ATLAS](https://atlas.cern) software installations.

It replaces the older [ayum](https://gitlab.cern.ch/atlas-sit/ayum) tool, which cannot be used on newer CentOS/RedHat versions >= 8.

The `atlas-dnf5` tool

  * can be used by a non privileged user
  * can relocate and install ATLAS and LCG RPM packages to an arbitrary location
  * uses its own private RPM database

[TOC]

## Quick Start

The `atlas-dnf5` tool can be used via [apptainer](https://apptainer.org), [podman](https://podman.io), [docker](https://docker.com) or natively (on EL 9 and clones only).
To use any of the container
versions, download the architecture independent `atlas-dnf5-<version>.tar.gz` file which contains the necessary wrapper scripts.
To use the native version download the `atlas-dnf5-<version>-<arch>.tar.gz` file.

Replace the version number in the following with the [latest release](https://gitlab.cern.ch/atlas-tdaq-software/atlas-dnf5/-/releases) or
the string `latest`. The latter is not guaranteed to work all the time.

### Install scripts

```shell
curl -L -O https://gitlab.cern.ch/api/v4/projects/141622/packages/generic/atlas-dnf5/v5.2.10.0/atlas-dnf5-v5.2.10.0.tar.gz
tar zxf atlas-dnf5-v5.2.10.0.tar.gz
export PATH=$(pwd)/atlas-dnf5-v5.2.10.0:$PATH
```

#### apptainer

If you've done the previous step, you are done. The `atlas-dnf5` script is a link to the `apptainer` specific wrapper.

If you have CVMFS and either

  * a RedHat like OS (e.g. Fedora, CentOS, ..), or
  * a local installation of `apptainer` (e.g. on Debian, Ubuntu, ...)

you can use the software directly from CVMFS:

```shell
command -v apptainer > /dev/null || alias apptainer=/cvmfs/atlas.cern.ch/repo/containers/sw/apptainer/x86_64-el9/current/bin/apptainer
alias atlas-dnf5='/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/atlas-tdaq-software/atlas-dnf5:v5.2.10.0/atlas-dnf5'
```

You can get the `apptainer` RPM or DEB packages for your system from the apptainer
[releases page](https://github.com/apptainer/apptainer/releases).


### podman

```shell
cd atlas-dnf5-v5.2.10.0
ln -sf atlas-dnf5.podman atlas-dnf5
```

This works both for `x86_64` and `aarch64` hosts as the images are multi arch.

### docker

```shell
cd atlas-dnf5-v5.2.10.0
ln -sf atlas-dnf5.docker atlas-dnf5
```

This works both for `x86_64` and `aarch64` hosts as the images are multi arch.

### Install Native Version on RedHat Enterprise Linux 9 and clones:

```shell
curl -L https://gitlab.cern.ch/api/v4/projects/141622/packages/generic/atlas-dnf5/v5.2.10.0/atlas-dnf5-x86_64-v5.2.10.0.tar.gz | tar zxf -
alias atlas-dnf5=$(pwd)/atlas-dnf5-x86_64-v5.2.10.0/atlas-dnf5
```

Replace `x86_64` with `aarch64` for the ARM version.

This variant may not be supported long-term as the upstream projects keeps using more and more newer package version
in its dependencies which are not available on RHEL 9.

## Configure

```shell
export ATLAS_DNF5_INSTALL_DIR=/path/to/install/area
export ATLAS_DNF5_STATE_DIR=/path/to/local/state/area
```

The first variable points to the desired installation area, and the second to a area where
`atlas-dnf5` keeps state information between invocations. Losing the latter is not critical
but prevents metadata caching, access to the history and information about which packages
are installed because of an user action, or as a dependency.

## Use

```shell
atlas-dnf5 repo list
atlas-dnf5 list Athena_*
atlas-dnf5 install -y tdaq-common-11-02-00_x86_64-el9-gcc13-opt
atlas-dnf5 list --installed
atlas-dnf5 history list
atlas-dnf5 leaves
```

## More Configurations

Note: The following are not available with the native EL9 version.

```shell
export ATLAS_DNF5_REPOS_DIR=/path/to/dir/with/repositories
```

The given directory will completely replace `/etc/yum.repos.d`

```shell
export ATLAS_DNF5_REPOS="myrepo.repo /path/to/myrepo2.repo"
```

The given repository files will be mounted in `/etc/yum.repo.d` in
addition to or overriding existing files.

```shell
export ATLAS_DNF5_HOST_INSTALL_DIR=/path/to/host/dir
```

This directory will be used on the host side while you can freely
choose the installation directory on the container side (which is
used in the post-install scripts run by RPM). A use case is where
you want to create the final installation area but your are on a host
where you don't have write access to that area. You can then tar up
the host area and untar it at the final host and destination.

For the `podman` and `docker` versions you can also use a simple string
as the host directory. It will be interpreted as container volume, so the installation
will be done into the volume.

## ATLAS Helper Scripts

The `atlas-find-project`, `atlas-install-project` (and `atlas-install-nightly`
if you have a use case for it) scripts should be in your path. Either have your
preferred `atlas-dnf5` wrapper on your `$PATH`, or set the `ATLAS_DNF5` variable
explicitly, e.g.

```shell
export ATLAS_DNF5=$HOME/bin/atlas-dnf5.podman
```

### `atlas-find-project`

Use `atlas-find-project` if you are not sure about the full exact name of the project
you try to install. For offline projects you should have the major and minor release
in the search string, for TDAQ projects it should contain the operating system name.

```shell
atlas-find-project Athena*24.0*
atlas-find-project Athena*24.0*aarch64*
atlas-find-project tdaq-11-02-00_x86_64*centos7*
atlas-find-project tdaq-11-02-00_aarch_64*el9*
```

### `atlas-install-project`

Use the `atlas-install-project` to install the project you want. If will automatically
enable all the required repositories for the offline, TDAQ and LCG stack. You can
pass global `dnf5` options like `-y` before the project name.

```shell
atlas-install-project Athena_24.0.20_x86_64-el9-gcc13-opt
atlas-install-project -y Athena_24.0.20_x86_64-el9-gcc13-opt
```

The script has two flags which will override the installation directory internally and
install the project to either the CVMFS or Point 1 default location.

```shell
# /cvmfs/atlas.cern.ch/repo/sw/software/24.0/Athena/24.0.20
atlas-install-project --cvmfs Athena_24.0.20_x86_64-el9-gcc13-opt

# /sw/atlas/Athena/24.0.20
atlas-install-project --point1 Athena_24.0.20_x86_64-el9-gcc13-opt
```

### `atlas-install-nightly`

If `ATLAS_DNF5_INSTALL_DIR` is not set, this script will default to the
`/cvmfs/atlas-nighltlies.cern.ch/repo/sw/nightlies/` location. It takes its
arguments either as four strings or as one string where all four arguments
are concatenated with a `/`. The arguments are:

  * the nightly branch (e.g. `main`)
  * the project (e.g. `Athena`)
  * the binary tag (e.g. `x86_64-el9-gcc13-opt`)
  * the date/time stamp (e.g. `2023-12-30T2101`)

You can the script in either of the following two equivalent ways:

```shell
atlas-install-nightly main Athena x86_64-el9-gcc13-opt 2023-12-30T2101
atlas-install-nightly main/Athena/x86_64-el9-gcc13-opt/2023-12-30T2101
```

By default it will also:

  * Remove the TDAQ directories from the installation area
  * Remove the project just installed from the RPM database

The latter makes sure that the next nightly can be installed into
the same area with a shared LCG installation, but a different time stamp.

If you don't want that you can prevent these actions by adding
the `--keep-tdaq` and/or `--keep-project` options.

## Advanced Topics

### Use with apptainer and local .sif file.

This assumes you have an apptainer installation. You might want to use this if you want
to e.g. modify the image for your needs, and, of course, if CVMFS is not available.

Get a local copy of the SIF image and extract and configure the
main script:

```shell
apptainer build atlas-dnf5.sif docker://gitlab-registry.cern.ch/atlas-tdaq-software/atlas-dnf5:v5.2.10.0
apptainer exec atlas-dnf5.sif get_script > atlas-dnf5
chmod a+x atlas-dnf5
alias atlas-dnf5=$(pwd)/atlas-dnf5
```

If you move the SIF file to another location, you can run the `get_script` again, or you can simply
change the apptainer image to be used at run time via the `ATLAS_DNF5_IMAGE` environment variable set
at the top of the script.

### Configuration

By default the installation area will contain the RPM database in
the top level `.rpmdb` subdirectory.

The state directory is optional but you will get a warning if not set.
It points to a state directory that will be
used for `/tmp`, `/var/tmp` and in turn for everything that
`dnf5` caches between executions, as well as various lock files.

If you don't point this to large enough local disk area, the
installation may fail. You can clean the area after a successfull
installation session, if you want - all necessary files will
be re-downloaded if needed.

All ATLAS and LCG package will be installed in the correct place and relation to each
other.

The structure of area will be as follows if a full TDAQ release is installed:

```
$ATLAS_DNF5_INSTALL_DIR/
             .rpmdb/
             tdaq-common/tdaq-common-11-02-00
             tdaq/tdaq-11-02-00
             sw/lcg/releases/LCG_104c
```

You can override dnf5 configuration entries via the `--setopt ...` command line argument to `atlas-dnf5`, but this can become tedious. You can
completely replace files in the container, or add new ones (e.g. for a new repository you need for tests).

```shell
# Create a new repository
vi new.repo

# Set custom options in dnf.conf
vi dnf.conf
export APPTAINER_BIND="$PWD/new.repo:/etc/yum.repos.d/new.repo,$PWD/dnf.conf:/etc/dnf/dnf.conf"
atlas-dnf5 list tdaq-common-11-01-00_WithoutLCG_x86-64-el9-gcc13-opt
```

### Repositories

The default installation contains the LCG repositories as well as _generic_ TDAQ and ATLAS offline
repositories that are parameterized via DNF variables. These variables have a
reasonable default and can be easily modified via the command line or environment variables.

The following variables are used in the repo configuration files:

    * OS     - default is el9. Selects the correct TDAQ repository.
    * BRANCH - default is 24.0. Selects the correct offline repository.
    * ARCH   - default is x86_64. Selects the correct LCG repositories.
    * LCG    - default is 104. Selects the correct LCG repositories.
    * DEBUG  - default is empty. Selects the correct LCG repositories for debug RPMS (only valid value is `debug`).

This gives you easy access to the latest releases without having a large
number of unused repositories in the configuration.

E.g. if you want a different offline branch, you can achieve this with either:

```shell
atlas-dnf5 --setvar BRANCH=24.2 install ...
```

or

```shell
export DNF_VAR_BRANCH=24.2
atlas-dnf5 install ...
```

### Shell Access to the Container

The following command will drop you into a shell in the container. Inside the container you have to
use the `dnf5` command instead of `atlas-dnf5`.

```shell
atlas-dnf5 --shell
dnf5 list --installed
```

This allows you to check the internals of the container if something does not
go as expected.

### Persistent Customizations with apptainer

For a native installation you can just modify the local copy of the `dist/etc/` files
directory. The following refers to the apptainer usage.

If you have a use case where you would like to customize the image and keep
the changes around, use an overlay:

```shell
apptainer overlay create --size 64 --create-dir /etc/dnf --create-dir /etc/yum.repos.d atlas-dnf5.sif
apptainer shell --writable atlas-dnf5.sif
# Make changes in /etc/dnf or /etc/yum.repos.d
vi /etc/dnf/dnf.conf
# Answer all questions e.g. from 'vi' that you really want to save it: it will
# appear it changed on disk while editing. Save it again to be sure...

# Create a new file
vi ~/test.repo
cp test.repo /etc/yum.repos.d/

# The changes will be there next time you run:
atlas-dnf5 install ...
```

Alternatively you can create a separate image to overlay. This allows you to keep
your customizations separate from the SIF image (e.g. in case of a new version) as
well as make it available easily to others.

```shell
apptainer overlay create --size 64 --create-dir /etc/dnf --create-dir /etc/yum.repos.d conf.img
export APPTAINER_OVERLAYIMAGE=$PWD/config.img`
apptainer shell atlas-dnf5.sif
vi /etc/dnf/dnf.conf
# Again, force a write after vi informs you the file has changed in the meantime
exit
# Changes will be there next time.
atlas-dnf5 install ...
```

### Use of podman/docker volumes

Instead of using host areas for the installation and state directories you can use podman/docker volumes
instead. You simply have to set one or both of the following variables

```shell
export ATLAS_DNF5_HOST_INSTALL_DIR=atlas-sw
export ATLAS_DNF5_STATE_DIR=atlas-sw-state
```

This has the advantage that you can install the software into a directory to which you have no access
on the host.

```shell
export ATLAS_DNF5_INSTALL_DIR=/sw/atlas
atlas-dnf5 install -y tdaq-11-02-00_x86_64-el9-gcc13-opt
```
You can now use this volume with e.g. the TDAQ system containers:

```shell
podman run -it --rm -v atlas-sw:/sw/atlas gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:el9
cm_setup tdaq-11-02-00
```

### Use of `atlas-dnf5` in Image Build Pipelines

You can use the tool as part of a build pipeline that creates inclusive images with the full
ATLAS/TDAQ/LCG stack. The following is an example of how the
[TDAQ image](https://gitlab.cern.ch/atlas-tdaq-software/admin/tdaq-release-container) is built:

```Dockerfile
ARG  ARCH=x86_64
ARG  OS=el9

FROM gitlab-registry.cern.ch/atlas-tdaq-software/atlas-dnf5:v5.2.10.0 as Builder
ARG  ARCH=x86_64
ARG  OS=el9
ARG  SW=gcc13-opt
ARG  RELEASE=tdaq-11-02-00

ENV  DNF_VAR_PREFIX=/sw/atlas
RUN  dnf5 install -y --enable-repo tdaq-updates --setvar ARCH=${ARCH} ${RELEASE}_${ARCH}-${OS}-${SW}
RUN  cd ${DNF_VAR_PREFIX}/tdaq && ln -sf ${RELEASE} prod
RUN  mkdir -p /sw/tdaq/ipc

FROM gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:${ARCH}-${OS}
ENV  TDAQ_IPC_INIT_REF=file:/sw/tdaq/ipc/init.ref
COPY --from=Builder /sw/atlas /sw/atlas
COPY --from=Builder /sw/tdaq/ipc /sw/tdaq/ipc
```

## Troubleshooting

A common problem is that dnf5 or rpm try to write to directories for which they have no permission.
In many cases you can change the locations via `dnf.conf`. Since we run apptainer
with the `--containall` option, the user's home directory and the root directory have
only a size of 64 MByte. Anything that needs larger (even temporary) disk space will
fail.

One example is the `atlas-dnf5 download ...` command which stores the RPM in
the current working directory (the home dir, which is a tmpfs). A work-around is

```shell
export APPTAINER_PWD=$HOME/download_dir
atlas-dnf5 download ...
```

Running the image uncontained is often an option, but has the possibility that it
will leave various files in your home directory. Check for `~/.local/share/dnf`,
`~/.cache/dnf`. In any case it is better to run the image fully contained as to
make the execution as reproducible as possible, and not dependent on your
specific shell environment, home dir layout etc.

Another option is to run with writable tmpfs as root to work around some of these issues:

```shell
export APPTAINER_WRITABLE_TMPFS=yes
./atlas-dnf5 install ...
```

## Implementation Details

### dnf5

We patch the DNF5 project in a similar way to the old yum.
Since all code is in C++ (no Python), it is not necessary to
patch `rpm` itself.

We introduce two new configuration variables for dnf:

  - `dbpath` - location of the RPM database. Only allowed in `[main]` section.
  - `prefix` - filesystem location to relocate to - allowed in both `[main]` and repo sections.
    Note that the prefix in the repo section can be a relative path. In this case the
    main prefix is prepended to it.

As a package is installed we read the RPM header to find all prefixes. We use
the `prefix` variable to set up a relocation table for the RPM, and pass it to
`rpmtsAddInstallElement()`.

The `dbpath` functionality is implemented via a plugin while the `prefix` functionality
unfortunately needs a patch to the DNF5 source itself.

The `dnf5` binary is built in a docker container (based on Fedora 39) and installed
into a minimal image. In addition a `/etc/dnf/dnf.conf` is provided with
custom settings, as well as several repositories in `/etc/yum.repos.d` for
the use in ATLAS (LCG, TDAQ, Offline). Note that all system repositories
are disabled.

The customized settings make sure that the container can execute both as `root`
and non-privileged user.

### RHEL/Alma/CentOS 9 Native Build

RHEL 9 lacks various packages and the `rpm` package itself is not
new enough. The `build.sh` script downloads and creates those manually.

Finally the `dnf5` binaries and libraries and needed `librpm*.so` libraries
are copied into a local area. A wrapper script `atlas-dnf5` sets a few
environment variables like `LD_LIBRARY_PATH` before it calls the internal
`dnf5` main executable.

**Warning:** Don't run the `build.sh` script anywhere except in a container you can
afford to throw away. It overwrites the system installation for `rpm`
in the build process with its own version (to be fixed).
