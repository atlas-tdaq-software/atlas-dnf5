FROM fedora:41 AS builder

ARG DNF5_BRANCH=atlas-branch-5.2.10.0

RUN dnf -y install git 'dnf-command(builddep)'
RUN git clone -b $DNF5_BRANCH https://github.com/rhauser/dnf5.git /var/tmp/dnf5
RUN ls /var/tmp/dnf5
RUN dnf -y builddep /var/tmp/dnf5/dnf5.spec
RUN mkdir /var/tmp/build && cd /var/tmp/build
RUN cmake \
 -D WITH_DNF5DAEMON_CLIENT=OFF \
 -D WITH_DNF5DAEMON_SERVER=OFF \
 -D WITH_DNF5DAEMON_TESTS=OFF  \
 -D WITH_DNF5_PLUGINS=OFF      \
 -D WITH_MAN=OFF               \
 -D WITH_PERFORMANCE_TESTS=OFF \
 -D WITH_PERL5=OFF             \
 -D WITH_PYTHON3=OFF           \
 -D WITH_PYTHON_PLUGINS_LOADER=OFF \
 -D WITH_PLUGIN_APPSTREAM=OFF  \
 -D WITH_RUBY=OFF              \
 -D WITH_SANITIZERS=OFF        \
 -D WITH_TESTS=OFF             \
 -D WITH_SYSTEMD=OFF           \
 /var/tmp/dnf5
RUN make -j $(nproc)
RUN make install

FROM fedora:41
RUN dnf -y install fmt python3 && dnf clean all
RUN dnf -y update
COPY --from=builder /usr/local/bin/dnf5 /usr/local/bin/dnf5
COPY --from=builder /usr/local/lib64/libdnf5* /usr/local/lib64/
COPY --from=builder /usr/local/lib64/libdnf5/plugins /usr/local/lib64/libdnf5/plugins
COPY --from=builder /usr/local/etc/dnf/libdnf5-plugins /etc/dnf/libdnf5-plugins
RUN  ldconfig /usr/local/lib64
RUN  rm -f /etc/yum.repos.d/fedora*
COPY dbpath.conf /etc/dnf/libdnf5-plugins/dbpath.conf
COPY dnf.conf    /etc/dnf/dnf.conf
COPY vars/*      /etc/dnf/vars/
COPY yum.repos.d/* /etc/yum.repos.d/
COPY atlas-dnf5 /atlas-dnf5
COPY get_script /usr/bin/get_script
ENTRYPOINT [ "/usr/local/bin/dnf5" ]
CMD        [ "--help" ]

