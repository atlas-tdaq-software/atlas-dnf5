#!/bin/bash
#
# Wrapper script for dnf5
#
# Environment variables to set before calling the script:
#
# ATLAS_DNF5_INSTALL_DIR - top level installation directory (required)
# ATLAS_DNF5_STATE_DIR   - state information for dnf5 (history, dependencies, etc. strongly suggested)
#
# ATLAS_DNF5_REPOS_DIR        - directory which will replace /etc/yum.repos.d completely (optional, expert)
# ATLAS_DNF5_REPOS            - list of space separated repo files that will be added to yum.repos.d (optional, expert)
# ATLAS_DNF5_HOST_INSTALL_DIR - installation directory on host, if different from target (optional, expert)
#

ATLAS_DNF5_IMAGE=${ATLAS_DNF5_IMAGE:-$(dirname $(readlink -f $0))}

[ -z "$ATLAS_DNF5_INSTALL_DIR" ] && { echo "Set ATLAS_DNF5_INSTALL_DIR to point to your install area !" >&2 ; exit 1; }
[ -z "$ATLAS_DNF5_STATE_DIR" ] && { echo "Warning: ATLAS_DNF5_STATE_DIR not set, will use /var/tmp/dnf-$(id -un) on host" >&2 ; }

export APPTAINERENV_DNF_VAR_PREFIX=${ATLAS_DNF5_HOST_INSTALL_DIR:-$ATLAS_DNF5_INSTALL_DIR}

${DNF_VAR_BRANCH:+export APPTAINERENV_DNF_VAR_BRANCH=${DNF_VAR_BRANCH}}
${DNF_VAR_OS:+export APPTAINERENV_DNF_VAR_OS=${DNF_VAR_OS}}
${DNF_VAR_ARCH:+export APPTAINERENV_DNF_VAR_ARCH=${DNF_VAR_ARCH}}
${DNF_VAR_LCG:+export APPTAINERENV_DNF_VAR_LCG=${DNF_VAR_LCG}}
${DNF_VAR_DEBUG:+export APPTAINERENV_DNF_VAR_DEBUG=${DNF_VAR_DEBUG}}
${DNF_VAR_OS:+export APPTAINERENV_DNF_VAR_OS_NUMBER=${DNF_VAR_OS_NUMBER:-$(echo -n ${DNF_VAR_OS} | tail -c 1)}}
${DNF_VAR_HTTP_PROXY:+export APPTAINER_DNF_VAR_HTTP_PROXY=${DNF_VAR_HTTP_PROXY}}

export APPTAINERENV_XDG_RUNTIME_DIR=${APPTAINERENV_XDG_RUNTIME_DIR:-/var/tmp/dnf-$(id -un)/run}

export APPTAINER_WORKDIR=${ATLAS_DNF5_STATE_DIR:-/var/tmp/dnf-state-$(id -un)}

export APPTAINER_BIND="${APPTAINER_BIND:+${APPTAINER_BIND},}${APPTAINERENV_DNF_VAR_PREFIX}${ATLAS_DNF5_REPOS_DIR:+,${ATLAS_DNF5_REPOS_DIR}:/etc/yum.repos.d}"
if [ -n "$ATLAS_DNF5_REPOS" ]; then
    for repo in $ATLAS_DNF5_REPOS
    do
        export APPTAINER_BIND="${APPTAINER_BIND},${repo}:/etc/yum.repos.d/$(basename ${repo})"
    done
fi
export APPTAINER_CONTAINALL=${APPTAINER_CONTAINALL:-true}
export APPTAINER_WRITABLE_TMPFS=${APPTAINER_WRITABLE_TMPFS:-true}

# Both directories must exist to be mounted into the container
mkdir -p $APPTAINERENV_DNF_VAR_PREFIX $APPTAINER_WORKDIR || exit 1

command=run
args="${ATLAS_DNF5_PROXY:+--setopt=proxy=${ATLAS_DNF5_PROXY}} $@"

[ "$1" = "--shell" ] && command=shell && args=""

for _arg in ${args}
do
    case ${_arg} in
        --setvar=OS=*)
            export APPTAINERENV_DNF_VAR_OS_NUMBER=$(echo -n ${_arg#--setvar=OS=} | tail -c 1)
            ;;
        OS=*)
	    export APPTAINERENV_DNF_VAR_OS_NUMBER=$(echo -n ${_arg#OS=} | tail -c 1)
	    ;;
    esac
done

${ATLAS_DNF5_APPTAINER:-$(command -v apptainer || echo /cvmfs/atlas.cern.ch/repo/containers/sw/apptainer/x86_64-el7/testing/bin/apptainer)} ${command} ${ATLAS_DNF5_IMAGE} $args
