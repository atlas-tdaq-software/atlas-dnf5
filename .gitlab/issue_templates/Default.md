## Summary

(summarize the issue concisely)

## What version of atlas-dnf5 do you use ?

(run `atlas-dnf5 --version`)

## How do you use atlas-dnf5 ?

  * [ ] using the CVMFS container via apptainer
  * [ ] from a local container via apptainer
  * [ ] from the Alma 9 tar file
  * [ ] via docker/podman container

(run `command -v atlas-dnf5` if you don't know)

## What version of apptainer are you using (if applicable) ?

(specify apptainer version, run `apptainer version`)

## From where do you use apptainer (if applicable) ?

  * [ ] CVMFS
  * [ ] Local install of apptainer 

## Describe the bug behavior

(what do you do and what is happening)

## What do you expect instead ?

(what should the behaviour be)

## Log files or output (if applicable)


